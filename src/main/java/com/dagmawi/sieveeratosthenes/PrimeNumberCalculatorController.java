package com.dagmawi.sieveeratosthenes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/prime")
@CrossOrigin(origins = "http://localhost:4200")
public class PrimeNumberCalculatorController {

	@Autowired
	PrimeService primeService;
	
	@GetMapping("/{inputNumber}")
	public ResponseEntity<List<Integer>> getPrimeNumber(@PathVariable int inputNumber) {
		
		List<Integer> primes = primeService.getPrimeNumber(inputNumber);
		
		return new ResponseEntity<>(primes, HttpStatus.OK);
	}
}
