package com.dagmawi.sieveeratosthenes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SieveEratosthenesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SieveEratosthenesApplication.class, args);
	}

}
