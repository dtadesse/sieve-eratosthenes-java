package com.dagmawi.sieveeratosthenes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class PrimeService {

	public List<Integer> getPrimeNumber(int inputNumber) {
		
		List<Integer> ansLis = new ArrayList<Integer>();
		
        boolean[] initialArray = new boolean[inputNumber + 1];
        Arrays.fill(initialArray, true);
        
        for(int i = 2; i < Math.sqrt(initialArray.length); i++){
            if(initialArray[i]){
                for (int j = i; j*i < initialArray.length; j++) {
                    initialArray[i*j] = false;
                }
            }
        }
        
        for(int i = 2; i < initialArray.length; i++){
            if(initialArray[i]){
                ansLis.add(i);
            }
        }
        
		return ansLis;
	}

}
